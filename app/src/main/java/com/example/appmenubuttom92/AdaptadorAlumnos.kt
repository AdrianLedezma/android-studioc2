package com.example.appmenubuttom92

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmenubuttom92.DataBase.Alumno
import com.google.android.material.imageview.ShapeableImageView

class AdaptadorAlumnos(
    private var alumnosList: List<Alumno>,
    private val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<AdaptadorAlumnos.AlumnoViewHolder>(), Filterable {

    private var filteredAlumnosList: List<Alumno> = alumnosList

    interface OnItemClickListener {
        fun onItemClick(alumno: Alumno)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlumnoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.alumno_item, parent, false)
        return AlumnoViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlumnoViewHolder, position: Int) {
        holder.bind(filteredAlumnosList[position], itemClickListener)
    }

    override fun getItemCount(): Int = filteredAlumnosList.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val query = constraint?.toString()?.lowercase()?.trim() ?: ""
                filteredAlumnosList = if (query.isEmpty()) {
                    alumnosList
                } else {
                    alumnosList.filter {
                        it.nombre.lowercase().contains(query) || it.especialidad.lowercase().contains(query)
                    }
                }
                return FilterResults().apply { values = filteredAlumnosList }
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredAlumnosList = results?.values as List<Alumno>? ?: alumnosList
                notifyDataSetChanged()
            }
        }
    }

    inner class AlumnoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val foto: ShapeableImageView = itemView.findViewById(R.id.foto)
        private val nombre: TextView = itemView.findViewById(R.id.txtAlumnoNombre)
        private val carrera: TextView = itemView.findViewById(R.id.txtAlumnoCarrera)
        private val matricula: TextView = itemView.findViewById(R.id.txtMatricula)

        fun bind(alumno: Alumno, clickListener: OnItemClickListener) {
            nombre.text = alumno.nombre
            carrera.text = alumno.especialidad
            matricula.text = alumno.matricula.toString()
            Glide.with(foto.context)
                .load(alumno.foto)
                .placeholder(R.mipmap.avatar)
                .into(foto)

            itemView.setOnClickListener { clickListener.onItemClick(alumno) }
        }
    }
}
