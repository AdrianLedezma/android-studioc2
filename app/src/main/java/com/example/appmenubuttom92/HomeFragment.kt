package com.example.appmenubuttom92

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.content.Intent
import android.net.Uri
import android.widget.ImageView
import android.widget.Toast

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val IconoListener = View.OnClickListener { contacto(it) }

        view.findViewById<ImageView>(R.id.IcoLinkedin).setOnClickListener(IconoListener)
        view.findViewById<ImageView>(R.id.IcoCorreo).setOnClickListener(IconoListener)
        view.findViewById<ImageView>(R.id.IcoInstagram).setOnClickListener(IconoListener)
    }

    private fun contacto(view: View) {
        val url = when (view.id) {
            R.id.IcoLinkedin -> "https://www.linkedin.com/in/AdrianLedezmaS"
            R.id.IcoCorreo -> "mailto:adrianledezmagm@gmail.com"
            R.id.IcoInstagram -> "https://www.instagram.com/adrian.ledezma21?igsh=MXE4YnpsazR3MXgyNA=="
            else -> ""
        }
        if (url.isNotEmpty()) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        } else {
            Toast.makeText(context, "URL no válida", Toast.LENGTH_SHORT).show()
        }
    }
}
