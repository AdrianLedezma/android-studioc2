package com.example.appmenubuttom92.DataBase

import android.os.Parcel
import android.os.Parcelable

data class Alumno(
    var id: Int = 0,
    var matricula: String = "",
    var nombre: String = "",
    var domicilio: String = "",
    var especialidad: String = "",
    var foto: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        id = parcel.readInt(),
        matricula = parcel.readString() ?: "",
        nombre = parcel.readString() ?: "",
        domicilio = parcel.readString() ?: "",
        especialidad = parcel.readString() ?: "",
        foto = parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        with(parcel) {
            writeInt(id)
            writeString(matricula)
            writeString(nombre)
            writeString(domicilio)
            writeString(especialidad)
            writeString(foto)
        }
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Alumno> {
        override fun createFromParcel(parcel: Parcel): Alumno = Alumno(parcel)
        override fun newArray(size: Int): Array<Alumno?> = arrayOfNulls(size)
    }
}
