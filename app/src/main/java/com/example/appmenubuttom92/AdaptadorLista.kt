import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Filter
import java.util.Locale

class AdaptadorLista(context: Context, private val items: List<String>) :
    ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, items) {

    private var filteredItems: List<String> = items

    override fun getCount(): Int = filteredItems.size

    override fun getItem(position: Int): String? = filteredItems[position]

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val query = constraint?.toString()?.lowercase(Locale.getDefault()) ?: ""
                filteredItems = if (query.isEmpty()) {
                    items
                } else {
                    items.filter { it.lowercase(Locale.getDefault()).contains(query) }
                }
                return FilterResults().apply {
                    values = filteredItems
                    count = filteredItems.size
                }
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredItems = results?.values as List<String>? ?: items
                notifyDataSetChanged()
            }
        }
    }
}
